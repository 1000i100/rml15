# Techniquement, comment ça marche ?

La June (Ǧ1), première monnaie libre (respectant la [TRM](http://www.creationmonetaire.info/2012/11/theorie-relative-de-la-monnaie-2-718.html))
repose sur le logiciel libre [Duniter](https://duniter.org/)
ayant donné naissance au [protocole DUP (Dividend Universel Protocol)](https://git.duniter.org/nodes/typescript/duniter/blob/dev/doc/Protocol.md)
encore [en évolution](https://git.duniter.org/nodes/common/doc/tree/ws2p_v2/rfc).

##### On peut la considérer comme une **crypto-monnaie** car sa fiabilité et sa cohérence reposent sur :
- des algorithmes asymétriques de hash et de signature numérique pour assurer l'intégrité des données,
- une **blockchain** pour éviter les doubles dépenses et obtenir un consensus global cohérent
  sur l'état actuel du système monétaire, alors qu'il n'y a aucune autorité centrale de référence
  mais un **réseau acentré** de nœuds dont la fiabilité n'est jamais garantie.

##### Cependant elle se différencie des crypto-monnaies classiques par les points suivants :
- L'unique mode de création monétaire est le DU (Dividende universel) :
  un montant égal crédité sur chaque compte membre quotidiennement.
  Tous les 6 mois, ce montant est réévalué pour maintenir
  l'augmentation régulière de la masse monétaire globale
  (d'environ 10% par an, ce qui la fait doubler en approximativement 7 ans)
  quelle que soit l'évolution du nombre de membres.
- **La masse monétaire n'est pas finie à terme, elle croît de manière exponentielle**.
- La création monétaire est **égale pour chaque membre, indépendemment des auteurs de blocs**
  (qui sont donc bénévoles ou rémunérés par [une surcouche de service de dons nommée Remuniter](https://forum.duniter.org/t/presentation-de-remuniter/1074), et non de manière systémique par création monétaire).
  Nous avons donc des forgerons qui forgent des blocs et non des mineurs qui les minent.
- En plus des transactions et de la création monétaire régulière, la blockchain gère également des certifications.
  Celles-ci servent à constituer une **toile de confiance** différenciant les comptes membres -
  bénéficiant du DU - des autres comptes, dits simples portefeuilles.
- Contrairement à celle du réseau GPG, ici **les certifications périment après 2 ans**.
  En effet, leur rôle n'est pas uniquement d'assurer l'appartenance d'une clef à un individu identifié,
  c'est aussi de s'assurer de l'**unicité d'un compte membre par individu vivant** identifié.
  En effet, la [licence Ǧ1](https://git.duniter.org/communication/licence-G1) est un contrat moral (voire légal) que chaque membre s'engage à respecter
  et à faire respecter aux membres et futurs membres qu'il certifiera.
  La péremption sert donc à ne pas garder membres les morts et à ne pas renouveler ceux qui contreviennent à la licence.
  Le choix politique de la sanction par non renouvellement plutôt que par révocation de certification
  a pour objectif de sécuriser les membres légitimes sur le fait qu'ils ne peuvent pas perdre leur statut de membre
  du jour au lendemain, sur un bad buzz, piratage ou autres déconvenues.
- Chaque bloc est signé par une clef membre, ce qui veut dire que seuls les membres peuvent forger des blocs.
  Il est donc possible de leur refuser les blocs suivants pour répartir les écritures de blocs entre membres,
  et ainsi garantir une gouvernance acentrée dans les écritures ajoutées en blockchain et la validation des blocs précédents.
  Tout cela sans besoin d'une course à la puissance de calcul.
- Un système de preuve de travail hybride (difficulté commune et difficulté personnalisée par membre selon son historique de création de bloc)
  est cependant utilisé pour obtenir un consensus sur l'heure courante et l'écoulement du temps.
  La difficulté de la preuve de travail s'ajuste automatiquement pour que soit créé en moyenne un bloc toutes les 5 minutes.
  Le consensus temporel est nécessaire tant pour la création régulière de DU que pour gérer l'obsolescence des certifications.

## Pour approfondir :
- [Article global sur le fonctionnement de Duniter](https://duniter.org/fr/comprendre/theorie/)
- [Article : Duniter, pourquoi, comment ?](https://duniter.org/fr/duniter-pourquoi-comment/)
- [Articles sur la toile de confiance](https://duniter.org/fr/category/toile-de-confiance/)
- [Article : Duniter est-il énergivore ?](https://duniter.org/fr/duniter-est-il-energivore/)
- [Article : L’écosystème logiciel](https://duniter.org/fr/ecosysteme-logiciel-duniter/)
- [Le forum technique ou explorer les sujets existants et poser vos questions](https://forum.duniter.org/)
